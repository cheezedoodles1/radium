"""Asynchronously fetches data from the given url & calculates its checksum."""
import time
import secrets
import string
from multiprocessing.dummy import Pool as ThreadPool

from checksumdir import dirhash
from git import Repo

GIT_PROJ_URL = 'https://gitea.radium.group/radium/project-configuration'
WORKERS = 3


def generate_name(length=5):
    """Generate a name of given length and return it as string.

    Parameters
    ----------
    length -- length of the generated name (default 5)

    Returns
    -------
    str: string
      generated string
    """
    return ''.join(secrets.choice(string.ascii_letters) for _ in range(length))


def fetch_parallel(func, url, name_func, workers=5):
    """Fetch contents given and return it as array.

    Parameters
    ----------
    func -- fetching function
    url -- url that is passed to func
    name_func -- function for folder name generation
    workers -- number of workers / folders

    Returns
    -------
    fetch_results: list of strings
      list of paths
    """
    pool = ThreadPool(workers)
    fetch_results = [
        pool.apply_async(func, args=(url, name_func())) for _ in range(workers)
    ]
    pool.close()
    pool.join()
    return fetch_results


def calculate_checksums(paths):
    """Calculate checksums of given list of directories.

    Parameters
    ----------
    paths -- list of Repo paths

    Returns
    -------
    list: list of strings
      list of checksums
    """
    return [
        dirhash(paths[num].get().working_dir, 'sha256') for num in range(len(paths))
        ]


if __name__ == '__main__':
    started = time.time()
    paths = fetch_parallel(
        Repo.clone_from, GIT_PROJ_URL, generate_name, WORKERS,
        )
    print(round(time.time() - started, 2), 'sec')
    checksums = calculate_checksums(paths)
