import os

class Dir:

    def __init__(self, dir_name):
        self.dir_name = dir_name

    @property
    def working_dir(self):
        print(self.dir_name)
        return os.path.abspath(self.dir_name)

def mock_generate_name(string):
    def func():
        return string
    return func
