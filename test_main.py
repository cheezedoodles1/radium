import os
import shutil

from git import Repo

from main import (generate_name, fetch_parallel,
                  calculate_checksums,
                  GIT_PROJ_URL, WORKERS,
                  )
from utils import Dir, mock_generate_name


def test_generate_name():
    assert len(generate_name(5)) == 5
    assert isinstance(generate_name(5), str)


def test_fetch_parallel():
    assert isinstance(fetch_parallel(
        Repo.clone_from, GIT_PROJ_URL,
        mock_generate_name('test_fetch'), 1,
            ), list,
        )
    # cleanup
    shutil.rmtree('./test_fetch')


def test_calculate_checksums():
    # using fetch_parallel to get paths list
    paths1 = fetch_parallel(
        Repo.clone_from, GIT_PROJ_URL, mock_generate_name('123'), 1,
        )
    paths2 = fetch_parallel(
      Repo.clone_from, GIT_PROJ_URL, mock_generate_name('321'), 1,
      )

    assert calculate_checksums(paths1) != calculate_checksums(paths2)
    assert calculate_checksums(paths1) == calculate_checksums(paths1)

    # cleanup
    shutil.rmtree('./123')
    shutil.rmtree('./321')


def test_calculate_checksums_with_mock():
    # create "fetched" directories and write files to them
    os.mkdir('./test1')
    with open('./test1/1.txt', 'w') as tmp_file:
        tmp_file.write('123')

    os.mkdir('./test2')
    with open('./test2/2.txt', 'w') as tmp_file:
        tmp_file.write('321')

    # create paths list with mocked Repo instances
    paths_mock1 = [Dir('./test1')]
    paths_mock2 = [Dir('./test2')]

    assert calculate_checksums(paths_mock1)[0] != calculate_checksums(paths_mock2)[0]
    assert calculate_checksums(paths_mock1)[0] == calculate_checksums(paths_mock1)[0]

    # cleanup
    shutil.rmtree('./test1')
    shutil.rmtree('./test2')
